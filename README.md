# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is just a basic demonstration of a web server built in Golang. The main objective
* Is to run the program and pull up the webpage. The index.html is purely a filler
* Version 1.0

### How do I get set up? ###

* Built in Golang 1.7.1. Expected to have reasonable backwards compatibility, but still 
* recommended to update to latest Go version
* From file in terminal, use command 'go build -o server'
* will compile and create executable object in the name of server
* run program with command, './server'
* Instructions in terminal will prompt user to open a web browser and input the address:
* 'localhost:8080'

### Who do I talk to? ###

* Author: Andrew Beaulieu
* Dec 20, 2016